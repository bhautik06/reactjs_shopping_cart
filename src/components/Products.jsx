import React, { useState } from "react";
import { Col, Row, Container, Card, Button, Table } from "react-bootstrap";
import Header from "./Header";
import { IoMdCart } from "react-icons/io";
import {
  FaCcVisa,
  FaCcMastercard,
  FaCcAmex,
  FaCcDiscover,
} from "react-icons/fa";
import cartdata from "./data/cartdata.json";
import { Modal } from "react-responsive-modal";
import "react-responsive-modal/styles.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "./styles/products.scss";

function Products() {
  const [total, setTotal] = useState(0);
  const [pro, setPro] = useState([]);
  const [amounts, setAmounts] = useState(0);
  const [open, setOpen] = useState();

  const increment = ({ id, name, desc, price, img }) => {
    setTotal((prevState) => prevState + price);
    setProducts({
      id: id,
      name: name,
      desc: desc,
      price: price,
      amount: 1,
      img: img,
    });
  };

  const setProducts = ({ id, name, desc, price, amount, img }) => {
    const allpro = pro;
    const findPro = pro.find((product) => product.id === id);
    if (findPro) {
      for (const product of allpro) {
        if (product.id === id) {
          product.amount = product.amount + 1;
          setAmounts(amounts + 1);
        }
      }
      setPro(allpro);
    } else {
      let proData = {
        id: id,
        name: name,
        desc: desc,
        price: price,
        amount: amount,
        img: img,
      };
      setPro([...pro, proData]);
      setAmounts(amounts + 1);
    }
  };

  const Inc = ({ id }) => {
    const product = pro;
    for (const products of product) {
      if (products.id === id) {
        products.amount = product.amount + 1;
        setAmounts(amounts + 1);
      }
    }
    setPro(product);
  };

  const onOpenModal = () => {
    setOpen(true);
  };

  const onCloseModal = () => {
    setOpen(false);
  };

  return (
    <>
      <Header
        total={pro.length}
        product={pro}
        amount={amounts}
        increment={() => Inc()}
      />
      <div className="products">
        <h1>Products</h1>
      </div>
      <Container>
        <Row className="rows" style={{ marginBottom: "2%" }}>
          {cartdata.map((cartDetails, index) => {
            return (
              <>
                <Col sm={3}>
                  <Card.Img variant="top" src={cartDetails.img} />
                  <Card.Body>
                    <Card.Title
                      style={{ fontWeight: "bold", marginBottom: "0px" }}
                    >
                      {cartDetails.itemName}
                    </Card.Title>
                    <Card.Text
                      style={{ fontStyle: "italic", marginBottom: "0px" }}
                    >
                      {cartDetails.desc}
                    </Card.Text>
                    <Card.Text style={{ fontWeight: "bold" }}>
                      ${cartDetails.price.toFixed(2)}
                    </Card.Text>
                    <Button
                      onClick={() =>
                        increment({
                          id: cartDetails.id,
                          name: cartDetails.itemName,
                          desc: cartDetails.desc,
                          price: cartDetails.price,
                          img: cartDetails.img,
                        })
                      }
                      variant="dark"
                      className="cartbutton"
                    >
                      Add to Cart
                    </Button>
                  </Card.Body>
                </Col>
              </>
            );
          })}
        </Row>
        {pro.length > 0 ? (
          <>
            <h2>Check Out area</h2>
            <span style={{ fontSize: "20px" }}>
              {amounts} <IoMdCart size="25px" />
            </span>
            <br />
            <br />
            <Table
              responsive
              align="center"
              borderless
              style={{ width: "50%" }}
            >
              <thead>
                <tr>
                  <th>SKU</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Amount</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                {pro.map((proDetail) => {
                  return (
                    <tr>
                      <td>{proDetail.id}</td>
                      <td>{proDetail.name}</td>
                      <td>{proDetail.desc}</td>
                      <td>{proDetail.amount}</td>
                      <td>${proDetail.price.toFixed(2)}</td>
                    </tr>
                  );
                })}
              </tbody>
              <tr>
                <td colSpan="5">
                  <hr />
                </td>
              </tr>
              <tr>
                <td colSpan="3" />
                <td align="right">SubTotal:</td>
                <td>${total.toFixed(2)}</td>
              </tr>
              <tr>
                <td colSpan="3" />
                <td align="right">Tax:</td>
                <td>${(total * 0.05).toFixed(2)}</td>
              </tr>
              <tr>
                <td colSpan="3" />
                <td align="right">
                  <b>Total:</b>
                </td>
                <td>
                  <b>${(total + total * 0.05).toFixed(2)}</b>
                </td>
              </tr>
            </Table>
            <Button
              style={{ marginBottom: "2%" }}
              variant="dark"
              className="cartbutton"
              onClick={onOpenModal}
            >
              Check Out
            </Button>
            <Modal open={open} onClose={onCloseModal} center>
              <br />
              <h3 align="center">
                <b>Checkout</b>
              </h3>
              <h6 align="center">
                We accept: <FaCcVisa size="20px" />{" "}
                <FaCcMastercard size="20px" /> <FaCcAmex size="20px" />{" "}
                <FaCcDiscover size="20px" />
              </h6>
              <br />
              <h4 align="center">
                <b>Subtotal: ${total.toFixed(2)}</b>
              </h4>
              <h4 align="center">
                <b>Tax: ${(total * 0.05).toFixed(2)}</b>
              </h4>
              <h2 align="center">
                <b>Total: ${(total + total * 0.05).toFixed(2)}</b>
              </h2>
              <br />
              <h6>This is where our payment processor goes</h6>
            </Modal>
          </>
        ) : (
          ""
        )}
      </Container>
    </>
  );
}

export default Products;
 