import React, { useState } from "react";
import { Card, Table } from "react-bootstrap";
import { IoMdCart } from "react-icons/io";
import { Modal } from "react-responsive-modal";
import "bootstrap/dist/css/bootstrap.min.css";
import "./styles/header.scss";

function Header(props) {
  const [open, setOpen] = useState();

  const onOpenModal = () => {
    setOpen(true);
  };

  const onCloseModal = () => {
    setOpen(false);
  };

  return (
    <div>
      <div className="headerImg">
        <Card className="cartbox" onClick={onOpenModal}>
          <Card.Text id="carttext">
            {props.amount}
            <IoMdCart size="20px" style={{ marginLeft: "5px" }} />
          </Card.Text>
        </Card>
        <Card id="title">Shopping Cart</Card>
      </div>
      <Modal
        focusTrapped
        open={open}
        onClose={onCloseModal}
        center
        classNames={{
          modal: "customModal",
        }}
      >
        {props.product.length > 0 ? (
          <div className="tables">
            <Table
              responsive
              align="center"
              borderless
              style={{ width: "100%", textAlign: "center" }}
            >
              <thead>
                <tr>
                  <th />
                </tr>
              </thead>
              <tbody>
                {props.product.map((proDetail) => {
                  return (
                    <tr>
                      <td>
                        <img
                          src={proDetail.img}
                          width="40px"
                          height="40px"
                          alt=""
                        />
                      </td>
                      <td>{proDetail.name}</td>
                      <td>
                        <button className="btn1">
                          <b>-</b>
                        </button>
                        {proDetail.amount}
                        <button
                          className="btn2"
                        >
                          <b>+</b>
                        </button>
                      </td>
                      <td>${proDetail.price.toFixed(2)}</td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </div>
        ) : (
          null
        )}
        <div>
          <br />
          <hr style={{ color: "#fff" }} />
          <h4 style={{ textAlign: "right" }}>${props.total.toFixed(2)}</h4>
        </div>
      </Modal>
    </div>
  );
}

export default Header;
